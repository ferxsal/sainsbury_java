import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.*;  



public class TestScrapeSainsbury {

	static ScrapeSainsbury tester;
	private static String mainpage = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html";
	private static WebDriver  driver;
	
	  @BeforeClass
	  public static void testSetup() {
	    
		tester = new ScrapeSainsbury();
	    tester.parseWebPage(mainpage); 
	    driver = new ChromeDriver();  
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  } 
	
	 @Test
	 public void testContactAutomationJava() throws Exception {
	      driver.get(mainpage);
	      assertEquals(driver.getTitle() ,"Ripe & ready | Sainsbury's");
	 }
	 
	@Test
     public void numberOfLinksProducts() {	
		assertEquals(7, tester.cartList.size()); 
     }
	
	@Test
    public void checkProductsScrapWeb() {
		
	List<Product> products = tester.cartList;
	float total = 0.0f;
	
	assertEquals( "3.50", products.get(0).getUnit_price());
	assertEquals( "1.50", products.get(1).getUnit_price());
	assertEquals( "1.80", products.get(2).getUnit_price());
	assertEquals( "3.20", products.get(3).getUnit_price());
	assertEquals( "1.50", products.get(4).getUnit_price());
	assertEquals( "1.80", products.get(5).getUnit_price());
	assertEquals( "1.80", products.get(6).getUnit_price());
	
	assertEquals( "Sainsbury's Apricot Ripe & Ready x5", products.get(0).getTitle());
	assertEquals( "Sainsbury's Avocado Ripe & Ready XL Loose 300g", products.get(1).getTitle());
	assertEquals( "Sainsbury's Avocado, Ripe & Ready x2", products.get(2).getTitle());
	assertEquals( "Sainsbury's Avocados, Ripe & Ready x4", products.get(3).getTitle());
	assertEquals( "Sainsbury's Conference Pears, Ripe & Ready x4 (minimum)", products.get(4).getTitle());
	assertEquals( "Sainsbury's Golden Kiwi x4", products.get(5).getTitle());
	assertEquals( "Sainsbury's Kiwi Fruit, Ripe & Ready x4", products.get(6).getTitle());
		
	for (Product p : products) 
      total = total + Float.parseFloat(p.getUnit_price());		
		
	 assertEquals( "15.1", String.valueOf(total)); 
	
    }
	
	@After
	public void tearDown() throws Exception {
	      driver.quit();
	}
}

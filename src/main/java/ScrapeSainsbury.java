import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup; 
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class ScrapeSainsbury {

	private static String mainpage = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html"; 
	static List<Product> cartList = new ArrayList<Product>(); 
	String Url;
	
	public ScrapeSainsbury(String Url){	
		this.Url = Url;
	}
	
    public ScrapeSainsbury(){
		
	}
	
    public static void parseWebPage(String url){
		
		 try {
		      
			 // fetch the document over HTTP
		      Document doc = Jsoup.connect(url).get();
		      Elements linksprods = doc.select("div.productInfo").select("a[href]");
		      String title, size, unit_price, description = null;
		      Document doclink = null;
		      float sizedoclink = 0.0f;
		      float total = 0.0f;
		      
		      for (Element link : linksprods) {	
		    	  
		    	    doclink = Jsoup.connect(link.attr("href")).get();
		    	    doclink.getElementsByClass("productTitleDescriptionContainer").select("h1").text();
		    	    Connection.Response response =  Jsoup.connect(link.attr("href")).execute();
		            sizedoclink = response.bodyAsBytes().length / 1024;  
		    	    
		            title = doclink.getElementsByClass("productTitleDescriptionContainer").select("h1").text();
		    	    size = String.valueOf(sizedoclink).concat("kb");
		            unit_price = doclink.getElementsByClass("pricePerUnit").first().text().replace("�", "").replace("/unit", "");
		            description =  doclink.getElementsByClass("productText").select("p").text();
		                           
		            cartList.add(new Product(title,String.valueOf(sizedoclink).concat("kb"),unit_price,description)  );
		            
		            total = total +  Float.parseFloat(unit_price);                      
		     }
		    
		      
		      JSONObject  jobj = getJsonFromMyFormObject(cartList,total);
			  //String niceFormattedJson = JsonWriter.formatJson(jsonString)
			  System.out.println( jobj.toString(3)); 

		    }   
		      catch (IOException e) {
		    	    e.printStackTrace();
		     }
		   		
		}
		
	
	public static JSONObject getJsonFromMyFormObject(List<Product> product,float total) {
	    JSONObject responseDetailsJson = new JSONObject();
	    JSONArray jsonArray = new JSONArray();
	    DecimalFormat df = new DecimalFormat("#.##");
	    
	    for (int i = 0; i < product.size(); i++) {
	        JSONObject formDetailsJson = new JSONObject();
	        formDetailsJson.put("title", product.get(i).getTitle());
	        formDetailsJson.put("size", product.get(i).getSize());
	        formDetailsJson.put("unit_price", product.get(i).getUnit_price());
	        formDetailsJson.put("description", product.get(i).getDescription());
	        
	        jsonArray.put(formDetailsJson);
	    }
    
	    responseDetailsJson.put("results", jsonArray);
	    responseDetailsJson.put("total", df.format(total).replace(",", "."));
	    
	    return responseDetailsJson;

	} 
	
	public static void main(String[] args) {
		
		ScrapeSainsbury.parseWebPage(mainpage);
		
	}
	   
}
